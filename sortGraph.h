/*! \file  sortGraph.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 8:34 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef SORTGRAPH_H
#define	SORTGRAPH_H

#ifdef	__cplusplus
extern "C"
{
#endif

#ifndef TFTHOST_H
#include "TFThost.h"
#endif


#define displayInitialize(fmt) {  putch(TFTINIT);  putch(fmt); idleSync(); }
#define rect(x1,y1,x2,y2) { putch(TFTRECT); sendWord(x1); sendWord(y1); sendWord(x2); sendWord(y2); }
//#define roundRect(x1,y1,x2,y2) { putch(TFTROUNDRECT); sendWord(x1); sendWord(y1); sendWord(x2); sendWord(y2); }
//#define filledRoundRect(x1,y1,x2,y2) { putch(TFTFILLROUNDRECT); sendWord(x1); sendWord(y1); sendWord(x2); sendWord(y2); }
//#define filledCircle(x,y,r) { putch(TFTFILLCIRCLE); sendWord(x); sendWord(y); sendWord(r); }
//#define circle(x,y,r) { putch(TFTCIRCLE); sendWord(x); sendWord(y); sendWord(r); }
#define printInt(val,fmt,x,y) { putch(TFTPRINTINT); sendWord(val); sendWord(fmt); sendWord(x); sendWord(y); }
//#define printDec(val,fmt,x,y) { putch(TFTPRINTDEC); sendWord(val); sendWord(fmt); sendWord(x); sendWord(y); }
//#define setColorRGB(r,g,b) { putch(TFTSETCOLOR); putch(r); putch(g); putch(b);}
//#define setBackColorRGB(r,g,b) { putch(TFTSETBACKCOLOR); putch(r); putch(g); putch(b);}
#define clearScreen() { putch(TFTCLEAR); }
//#define transitionOpen(c) { putch(TFTTRANSITIONOPEN); sendWord(c); }
//#define transitionClose(c) { putch(TFTTRANSITIONCLOSE); sendWord(c); }
#define sendWord(word) {putch(word&0xff);putch((word&0xff00)>>8);}
#define putChar(x,y,ch) {putch(TFTPRINTCHAR);putch(ch);sendWord(x);sendWord(y);}
#define setColorX(color) {putch(TFTSETCOLORX);sendWord(color);}
#define setBackColorX(color) {putch(TFTSETBACKCOLORX);sendWord(color);}
#define line(x1,y1,x2,y2) {putch(TFTLINE);sendWord(x1);sendWord(y1);sendWord(x2);sendWord(y2);}
#define setFont(font) {putch(TFTSETFONT);putch(font);}
#define filledRect(x1,y1,x2,y2) {putch(TFTFILLRECT);sendWord(x1);sendWord(y1);sendWord(x2);sendWord(y2);}



void putString(int, int, int, char *);
void serialInitialize( int );
void putch( unsigned char );
void idleSync(void);
void waitAshort(int);
void waitAbit(int);


#ifdef	__cplusplus
}
#endif

#endif	/* SORTGRAPH_H */

