/*! \file  graphInternal.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 10:19 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPHINTERNAL_H
#define	GRAPHINTERNAL_H

#ifdef	__cplusplus
extern "C"
{
#endif

#ifndef EXTERN
#define EXTERN extern
#endif

EXTERN unsigned int uScreenColor;
EXTERN unsigned int uFieldColor;
EXTERN unsigned int uAnnotColor;
EXTERN unsigned int uXLabelColor;
EXTERN unsigned int uYLabelColor;
EXTERN unsigned int uTitleColor;
EXTERN unsigned int uLineColor;
EXTERN unsigned int uCursorColor;
EXTERN int nXmin;
EXTERN int nXmax;
EXTERN int nYmin;
EXTERN int nYmax;
EXTERN char szTitle[32];
EXTERN char szXLabel[32];
EXTERN char szYLabel[32];
EXTERN int nLastX,nLastY;


void annotateY( void );



#ifdef	__cplusplus
}
#endif

#endif	/* GRAPHINTERNAL_H */

