/*! \file  UART.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 8:31 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "sortGraph.h"


/* Baud rate generator calculations */
//! On-board Crystal frequency
#define XTFREQ          8000000
//! On-chip PLL setting
#define PLLMODE         2
//! Instruction Cycle Frequency
#define FCY             XTFREQ*PLLMODE/2
//! Desired baud rate
#define BAUDRATE         38400
//! Value for the baud rate generator
#define BRGVAL          ((FCY/BAUDRATE)/16)-1


/*! Initialize the UART */
/*! serialInitialize() initializes UART1 to the selected baud rate and the
 *  desired set of pins.
 *
 * \param bAlternate int 0 to use default UART pins, 1 to use alternate pins
 * \returns none
 */
void serialInitialize( int bAlternate )
{
      // UART setup
    U1BRG  = BRGVAL;        // Set up baud rate generator
    if ( bAlternate )       // Alternate pins selected?
        U1MODE = 0x8400;    // Reset UART to 8-n-1, alt pins, and enable
    else
        U1MODE = 0x8000;    // Reset UART to 8-n-1, primary pins, and enable
    U1STA  = 0x0440;        // Reset status register and enable TX & RX
    _U1RXIF=0;              // Clear UART RX Interrupt Flag
}

/*! Send a character over the serial port */
/*! putchSerial() sends a single character over the serial port.
 *
 * \param ch unsigned char - character to send
 * \returns none
 */
void putch( unsigned char ch )
{
    while(U1STAbits.UTXBF); // Wait if buffer full
    while(!U1STAbits.TRMT); // Wait for prev char to complete
    U1TXREG = ch;                   // char to UART xmit register
//    waitAshort(4);
}


/*! Send a bunch of nulls to guarantee sync with terminal */
void idleSync(void)
{
  int i;
  for ( i=0; i<20; i++ )
    {
      putch(0);
      waitAshort(10);
    }
}


/*! Wait a fairly long time */
void waitAbit( int len )
{
  int i,j;

  _LATB9 = 0;
  for ( j=0; j<len; j++ )
    for (i=0; i<32000; i++ )
        ;
  _LATB9 = 1;
}

/*! Wait a short time */
void waitAshort( int len )
{
  int i,j;

  for ( j=0; j<len; j++ )
    for (i=0; i<1000; i++ )
        ;
}

