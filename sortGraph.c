/*! \file  sortGraph.c
 *
 *  \brief Sort out how we will implement graphing system
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 8:29 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdlib.h>
#include <math.h>
#include "TFThost.h"
#include "colors.h"
#include "sortGraph.h"
#include "graph.h"

#define EXTERN
#include "graphInternal.h"

//#define FAST
#ifdef FAST
#define END_OF_SCREEN_DELAY 0
#else
#define END_OF_SCREEN_DELAY 200
#endif

#define TFTGINIT        0x19
#define TFTGADDPT       0x1a
#define TFTGEXPOSE      0x1b
#define TFTGCOLORS      0x1c
#define TFTGRANGE       0x1d
#define TFTGTITLE       0x1e
#define TFTGXLABEL      0x1f
#define TFTGYLABEL      0x20
#define TFTGNOCURSOR    0x21
#define TFTGNOERASE     0x22

void TFTGinit( void )
{
  putch(TFTGINIT);
}

void TFTGexpose( void )
{
  putch(TFTGEXPOSE);
}

void TFTGcolors(unsigned int ctit,unsigned int cann,unsigned int cxlab,unsigned int cylab,unsigned int cscreen,unsigned int cfield,unsigned int cdata)
{
  putch(TFTGCOLORS);
  sendWord(ctit);
  sendWord(cann);
  sendWord(cxlab);
  sendWord(cylab);
  sendWord(cscreen);
  sendWord(cfield);
  sendWord(cdata);
}

void TFTGaddPoint(int y)
{
  putch(TFTGADDPT);
  sendWord(y);
}

void TFTGrange(int nX0, int nX9, int nY0, int nY9 )
{
  putch(TFTGRANGE);
  sendWord(nX0);
  sendWord(nX9);
  sendWord(nY0);
  sendWord(nY9);
}

void TFTGtitle( char *p )
{
  int i;

  putch(TFTGTITLE);
  for ( i=0; i<23; i++ )
    {
      putch( *p );
      p++;
    }
  putch(0);
}

void TFTGYlabel( char *p )
{
  int i;

  putch(TFTGYLABEL);
  for ( i=0; i<23; i++ )
    {
      putch( *p );
      p++;
    }
  putch(0);
}

void TFTGXlabel( char *p )
{
  int i;

  putch(TFTGXLABEL);
  for ( i=0; i<23; i++ )
    {
      putch( *p );
      p++;
    }
  putch(0);
}

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer

char Yannot[] = "Y-Axis Annotation";

#define OLDSTYLE

/*! sortGraph - */

/*!
 *
 */
int main(void)
{
  int i;
  int x,y;

  // LED will be used for general signaling
  _TRISB9 = 0;

  waitAbit(100);

  serialInitialize(0);

  // Sync
  for (i = 0; i < 15; i++)
    putch(0);

  // Initialize TFT
  displayInitialize(TFTLANDSCAPE);


  while (1)
    {
      // Graph without running TFTGINIT
      setBackColorX(FORESTGREEN);
      clearScreen();
      
      TFTGinit();   // OK, we lied

      TFTGexpose();

      TFTGcolors(WHITE,BLACK,GREENYELLOW,GREENYELLOW,FORESTGREEN,BURLYWOOD,DARKRED);

      TFTGrange(0,275,87,97);
      //setLabels("Sine function",0,0);
      TFTGtitle("Graph One - Leftovers");
      TFTGexpose();


      for ( x=0; x<554; x++ )
        {
          y = (int) (60.0 + 30.0*sin((double)x/10.0)+rand()/512);
          TFTGaddPoint(y);
          //waitAshort(50);
        }
      waitAbit(END_OF_SCREEN_DELAY);
 //while(1);

      // Default colors
      TFTGinit();
      TFTGrange(0,500,90,190);
      //setLabels("Graph Title","X-axis label","Y Label");
      TFTGtitle("Graph Two");
      TFTGXlabel("X-axis label");
      TFTGYlabel("Y Label");
      TFTGexpose();
      {
        double fX, fY;
        for (x = 0; x < 277; x++)
          {
            fX = (double)x *0.1 - 15.0;
            fY = ((fX*fX*fX)/29.0)+120.0;
            TFTGaddPoint((int)fY);
          }
      }
      waitAbit(END_OF_SCREEN_DELAY);

      TFTGinit();
      //setLabels("Sine function","X Value","Y Value");
      TFTGtitle("Graph Three");
      TFTGXlabel("X Value");
      TFTGYlabel("Y Value");
      TFTGcolors(BLACK,BLACK,BLACK,BLACK,WHITE,DARKCYAN,WHITE);
      TFTGrange(0,275,0,9);
      TFTGexpose();
      for ( x=0; x<554; x++ )
        {
          y = (int) (100.0 + 50.0*sin((double)x/10.0));
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);

      TFTGinit();
      //setLabels("Sine function","X Value","Y Value");
      TFTGtitle("Graph Four");
      TFTGXlabel("X Value");
      TFTGYlabel("Y Value");
      putch(TFTGNOCURSOR);
      TFTGcolors(WHITE,WHITE,WHITE,WHITE,BLACK,PALEGREEN,BLACK);
      TFTGrange(0,7,0,20);
      TFTGexpose();
      for ( x=0; x<554; x++ )
        {
          y = (int) (100.0 + 50.0*sin((double)x/10.0));
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);


      TFTGinit();
      //setLabels("Sine function","X Value","Y Value");
      TFTGtitle("Graph Five");
      TFTGXlabel("X Value");
      TFTGYlabel("Y Value");
      TFTGcolors(WHITE,ROYALBLUE,DEEPSKYBLUE,DEEPSKYBLUE,NAVY,WHITE,RED);
      TFTGrange(100,400,0,25);
      TFTGexpose();
      for ( x=0; x<277; x++ )
        {
          y = (int) (100.0 + 50.0*sin((double)x/30.0));
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);

      TFTGinit();
      //setLabels("Sine function","X Value","Y Value");
      TFTGtitle("Graph Six");
      TFTGXlabel("- 6 X Value 6 -");
      TFTGYlabel("Y Value");
      TFTGcolors(BLACK,BLACK,BLACK,BLACK,GRAY90,WHITE,BLACK);
      TFTGrange(0,275,0,1500);
      putch(TFTGNOERASE);
      TFTGexpose();
      for ( x=0; x<554; x++ )
        {
          y = (int) (100.0 + 50.0*sin((double)x/10.0));
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);

      TFTGinit();
      //setLabels("Sine function","X Value","Y Value");
      TFTGtitle("Graph Seven");
      TFTGXlabel("-- X Value --");
      TFTGYlabel("Y Value");
      TFTGcolors(BLACK,BLACK,BLACK,BLACK,GRAY,WHITE,BLACK);
      TFTGrange(0,275,0,9000);
      TFTGexpose();
      for ( x=0; x<277; x++ )
        {
          y = (int) (100.0 + 50.0*sin((double)x/20.0));
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);


      TFTGinit();
      //setLabels("Sine function","X Value","Y Value");
      TFTGtitle("Graph Eight");
      TFTGXlabel("X Value");
      TFTGYlabel("Y Value");
      TFTGcolors(BLACK,PERU,SIENNA,SIENNA,BURLYWOOD,WHITE,SADDLEBROWN);
      TFTGrange(0,80,0,4);
      TFTGexpose();
      for ( x=0; x<277; x++ )
        {
          y = (int) (100.0 + 50.0*sin((double)x/10.0)+rand()/4096);
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);

      TFTGinit();
      TFTGrange(0,500,0,100);
      //setLabels("Y=50sin(Theta/30)","30*Theta","Y+70");
      TFTGtitle("Y=50sin(Theta/30)");
      TFTGXlabel("- 9 X Value 9 -");
      TFTGYlabel("9 Y Value 9");
      TFTGcolors(GOLD,LIGHTSEAGREEN,GOLDENROD,GOLDENROD,GRAY4,BLACK,CHOCOLATE);
      TFTGexpose();
      for ( x=0; x<554; x++ )
        {
          y = (int) (70.0 + 50.0*sin((double)x/30.0));
          TFTGaddPoint(y);
        }
      waitAbit(END_OF_SCREEN_DELAY);

    }
  return 0;
}
