/*! \file  TFTGexpose.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:52 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "graph.h"
#include "graphInternal.h"
#include "sortGraph.h"


/*! TFTGexpose - */

/*!
 *
 */
void TFTGexpose( void )
{
  int i;
  int pos;
  int space;

  setBackColorX(uScreenColor);
  clearScreen();

  if (szTitle[0])
    {
      setFont(FONTBIG);
      setColorX(uTitleColor);
      if ( strlen(szTitle)>21 )
        pos = 160 - (13*strlen(szTitle))/2;
      else
        pos = 180 - (13*strlen(szTitle))/2;
      space = 13;
      if(strlen(szTitle)>24)
        {
          space = 12;
          pos = 0;
        }
      putString(pos, 2, space, szTitle);
    }
  setFont(FONTSMALL);

  if (szXLabel[0])
    {
      setColorX(uXLabelColor);
      pos = 180 - (7 * strlen(szXLabel)) / 2;
      putString(pos, 227, 7, szXLabel);
    }

  if (szYLabel[0])
    {
      setColorX(uYLabelColor);
      pos = 120 - 5 * strlen(szYLabel);
      for (i = 0; i < strlen(szYLabel); i++)
        {
          putChar(1, (pos + 10 * i), szYLabel[i]);
        }
    }

  setColorX(uAnnotColor);
//  printInt(2000, TFTPRINTINTRIGHT, 318, 218);
//  printInt(1000, TFTPRINTINTPLAIN, 164, 218);
//  printInt(0, TFTPRINTINTPLAIN, 38, 218);
  if ( nXmax>nXmin )
    annotateX();
  if ( nYmax>nYmin )
    annotateY();
/*
  for (i = 0; i < 4; i++)
    {
      printInt((3000 - 1000 * i), TFTPRINTINTRIGHT, 39, (20 + 62 * i));
    }
*/
  setColorX(uFieldColor);
  filledRect(41, 25, 319, 215);

}
