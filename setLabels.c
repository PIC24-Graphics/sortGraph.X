/*! \file  setLabels.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:51 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#include "graph.h"
#include "graphInternal.h"
#include "sortGraph.h"



void TFTGtitle( char *t )
{
  if ( t )
    strcpy(szTitle,t);
}

void TFTGXlabel( char *x )
{
  if (x)
    strcpy(szXLabel, x);
}

void TFTGYlabel( char *y )
{
  if (y)
    strcpy(szYLabel, y);
}
