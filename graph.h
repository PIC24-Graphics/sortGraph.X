/*! \file  graph.h
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 10:19 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef GRAPH_H
#define	GRAPH_H

#ifdef	__cplusplus
extern "C"
{
#endif

void TFTGinit(void);
void TFTGexpose(void);
//void setLabels(char *, char *, char *);
void TFTGcolors( unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int, unsigned int );
void TFTGrange( int, int, int, int );
void annotateX(void);
void annotateY(void);
void TFTGaddPoint( int );
void TFTGtitle( char * );
void TFTGXlabel( char * );
void TFTGYlabel( char * );

#ifdef	__cplusplus
}
#endif

#endif	/* GRAPH_H */

