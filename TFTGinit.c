/*! \file  TFTGinit.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:48 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <string.h>
#define EXTERN
#include "graphInternal.h"


/*! TFTGinit - */

/*!
 *
 */
void TFTGinit( void )
{
  uScreenColor = 0x2104;
  uFieldColor  = 0x0000;
  uAnnotColor  = 0x0451;
  uXLabelColor = 0x2595;
  uYLabelColor = 0x2595;
  uTitleColor  = 0xfea0;
  uLineColor   = 0xcc27;
  nXmin = 0;
//  nXmax = 279;
  nXmax = 0;
  nYmin = 0;
//  nYmax = 190;
  nYmax = 0;
  memset(szTitle,0,sizeof(szTitle));
  memset(szXLabel,0,sizeof(szXLabel));
  memset(szYLabel,0,sizeof(szYLabel));
  nLastX = 0;

}

