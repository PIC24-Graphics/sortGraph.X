/*! \file  graph.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 10:20 AM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "graph.h"
#include "graphInternal.h"
#include "sortGraph.h"

int calcPower(int nR)
{
  return (int) ( ceil( log10((double)nR) ) );
}

int iPow( int x, int y )
{
  double fPow;

  fPow = ceil(pow( (double)x, (double)y ));
  return (int) fPow;
}

void annotateY( void )
{
  int nRange;
  int nPower;
  int nScalePower;
  int nSteps;
  int nStepSize;
  int nThisVal;
  int nThisLoc;
  int i;
  int nTickedRange;
  int nPelsPerTic;

  nRange = nYmax - nYmin; // 190
  nPower = calcPower( nRange ); // 2
  nScalePower = nPower-1;
  nStepSize = ( iPow(10,nScalePower) ); // 10
  nSteps = nRange / nStepSize; //19
  if ( nSteps<2 )
    {
      nStepSize = ( iPow(10,nScalePower) )/5; // 10
      nSteps = nRange / nStepSize; //19
    }
  if ( nSteps>10 )
    {
      nStepSize = 2 * ( iPow(10,nScalePower) ); // 20
      nSteps = nRange / nStepSize; // 9
      if ( nSteps>10 )
        {
          nStepSize = 5 * ( iPow(10,nScalePower) );  // 50
          nSteps = nRange / nStepSize; // 3
        }
    }

  nTickedRange = nSteps*nStepSize;
  nPelsPerTic = (int)(190L*(long)nTickedRange/(long)nRange)/nSteps;

  for ( i=0; i<nSteps+1; i++ )
    {
      nThisVal = nYmin + i*nStepSize;
      nThisLoc = 215-i*nPelsPerTic;
      line(38,nThisLoc,41,nThisLoc);
      printInt(nThisVal, TFTPRINTINTRIGHT, 39, (nThisLoc-6));
    }
}

void annotateX( void )
{
  int nRange;
  int nPower;
  int nScalePower;
  int nSteps;
  int nStepSize;
  int nThisVal;
  int nThisLoc;
  int i;
  int nTickedRange;
  int nPelsPerTic;
  int nOffset;

  nRange = nXmax - nXmin;
  nPower = calcPower( nRange );
  nScalePower = nPower-1;
  nStepSize = ( iPow(10,nScalePower) );
  nSteps = nRange / nStepSize;
  if ( nSteps<2 )
    {
      nStepSize = ( iPow(10,nScalePower) )/2;
      nSteps = nRange / nStepSize;
    }
  if ( nSteps>10 )
    {
      nStepSize = 2 * ( iPow(10,nScalePower) );
      nSteps = nRange / nStepSize;
      if ( nSteps>10 )
        {
          nStepSize = 5 * ( iPow(10,nScalePower) );
          nSteps = nRange / nStepSize;
        }
    }

  nTickedRange = nSteps*nStepSize;
  nPelsPerTic = (int)(279L*(long)nTickedRange/(long)nRange)/nSteps;

  for ( i=0; i<nSteps; i++ )
    {
      nThisVal = nYmin + i*nStepSize;
      nThisLoc = 40+i*nPelsPerTic;
      line(nThisLoc,210,nThisLoc,220);
      nOffset = (int)(4.0*log10((double)nThisVal))+4;
      printInt(nThisVal, TFTPRINTINTPLAIN, (nThisLoc-nOffset),218);
    }
}


