/*! \file  TFThost.h
 *
 *  \brief Constants related to the TFT library
 *
 *
 *  \author jjmcd
 *  \date June 5, 2015, 3:42 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef TFTHOST_H
#define	TFTHOST_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! Initialize TFT */
#define TFTINIT            0x01
/*! Set color RGB */
#define TFTSETCOLOR        0x02
/*! Set Background Color RGB */
#define TFTSETBACKCOLOR    0x03
/*! Set color 16-bit */
#define TFTSETCOLORX       0x04
/*! Set background color 16-bit */
#define TFTSETBACKCOLORX   0x05
/*! Clear the screen */
#define TFTCLEAR           0x06
/*! Print teletype mode */
#define TFTPRINTTT         0x07
/*! Draw a rectangle */
#define TFTRECT            0x08
/*! Clear a rectangle */
#define TFTCLEARRECT       0x09
/*! Draw a pixel */
#define TFTPIXEL           0x0A
/*! Draw a line */
#define TFTLINE            0x0B
/*! Set the font */
#define TFTSETFONT         0x0C
/*! Print a character */
#define TFTPRINTCHAR       0x0D
/*! Draw a rounded rectangle */
#define TFTROUNDRECT       0x0E
/*! Draw a filled rounded rectangle */
#define TFTFILLROUNDRECT   0x0F
/*! Draw a circle */
#define TFTCIRCLE          0x10
/*! Draw a filled circle */
#define TFTFILLCIRCLE      0x11
/*! Draw a filled rectangle */
#define TFTFILLRECT        0x12
/*! Print a number with a decimal */
#define TFTPRINTDEC        0x13
/*! Print a number with a decimal right justified */
#define TFTPRINTDECRIGHT   0x14
/*! Print an integer */
#define TFTPRINTINT        0x15
/*! Fill screen starting at center */
#define TFTTRANSITIONOPEN  0x16
/*! Fill screen starting at edges */
#define TFTTRANSITIONCLOSE 0x17
/*! Wipe a new color on to the screen  */
#define TFTTRANSITIONWIPE  0x18


/*! Initialize - set mode to portrait */
#define TFTPORTRAIT      0x00
/*! Initialize - set mode to landscape */
#define TFTLANDSCAPE     0x01

/*! Set Font - Small font */
#define FONTSMALL        0x01
/*! Set Font - Big font */
#define FONTBIG          0x02
/*! Set Font - Seven segment number-only font */
#define FONTSEVENSEGNUM  0x03
/*! Set Font - Deja Vu Sans */
#define FONTDJS          0x04
/*! Set Font - Deja Vu Sans Bold */
#define FONTDJSB         0x05
/*! Set Font - Smallbold */
#define FONTBOLDSMALL    0x06

/*! PrintInt - Left justified, decimal */
#define TFTPRINTINTPLAIN 0x0000
/*! PrintInt - Left justified, hexadecimal*/
#define TFTPRINTINTHEX   0x0001
/*! PrintInt - Right justify */
#define TFTPRINTINTRIGHT 0x0002
/*! PrintInt - Zero fill */
#define TFTPRINTINTZERO  0x0004


#ifdef	__cplusplus
}
#endif

#endif	/* TFTHOST_H */

