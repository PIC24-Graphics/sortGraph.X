/*! \file  addPoint.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date June 16, 2015, 6:44 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "graph.h"
#include "graphInternal.h"
#include "sortGraph.h"


/*! addPoint - */

/*!
 *
 */
void TFTGaddPoint(int y)
{
  if ( y<0 )
    y=0;
  if ( y>190 )
    y=190;
  if ( nLastX!=0 )
    {
      if ( (nLastX+42) < 318 )
        {
          setColorX((uFieldColor^0xffff));
          line((nLastX+42),25,(nLastX+42),215);
        }
      setColorX(uFieldColor);
      line((nLastX+41),25,(nLastX+41),215);
      setColorX(uLineColor);
      line( (nLastX+40), (215-nLastY), (nLastX+41), (215-y) );
    }
  else
    {
      setColorX(uFieldColor);
      line((41),25,(41),215);
    }

  nLastY=y;
  nLastX++;
  if ( nLastX>276 )
    nLastX = 0;
}
